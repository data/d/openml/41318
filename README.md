# OpenML dataset: glass-0-1-4-6_vs_2

https://www.openml.org/d/41318

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

A imbalanced version of the Glass data set. where the possitive examples belong to classes 1. 2. 5 and 7 and the negative examples belong to the class 3. (IR: 11.06)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41318) of an [OpenML dataset](https://www.openml.org/d/41318). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41318/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41318/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41318/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

